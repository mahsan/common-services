package com.ahsan.commons.services;

import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import org.mockito.Mock;

import com.ahsan.common.services.ConfigProvider;

public class ConfigProviderTest {

	@Mock
	InputStream mockInputStream;

	@Test(expected = NullPointerException.class)
	public void test1() throws IOException {
		final ConfigProvider provider = new ConfigProvider(mockInputStream);
		provider.getProperties();
	}

	@Test
	public void test2() throws IOException {

		final InputStream inputStream = new ByteArrayInputStream("key=value".getBytes());
		final ConfigProvider provider = new ConfigProvider(inputStream);
		assertNotNull(provider.getProperties());
	}

}
