package com.ahsan.common.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class ConfigProvider {
	private final Map<String, String> properties;

	public ConfigProvider(final InputStream input) throws IOException {
		if (input == null) {
			throw new NullPointerException("Input stream is null");
		}
		properties = loadProperties(input);
	}

	private Map<String, String> loadProperties(final InputStream input) throws IOException {
		final Map<String, String> propertiesMap = new HashMap<>();
		final Properties prop = new Properties();
		prop.load(input);

		final Enumeration<?> e = prop.propertyNames();
		while (e.hasMoreElements()) {
			final String key = (String) e.nextElement();
			final String value = prop.getProperty(key);
			propertiesMap.put(key, value);
		}

		input.close();
		return propertiesMap;
	}

	public Map<String, String> getProperties() {
		if (properties == null) {
			return new HashMap<>();
		}
		return properties;
	}

}
